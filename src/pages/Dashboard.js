import React, { useContext, useEffect, useState } from 'react'
import Cookie from 'js-cookie'
import AuthApi from '../AuthContext'
import axios from 'axios'
import jwtDecode from 'jwt-decode'
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import { Avatar } from '@material-ui/core'
import { deepOrange, deepPurple } from '@material-ui/core/colors'

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    orange: {
        color: theme.palette.getContrastText(deepOrange[500]),
        backgroundColor: deepOrange[500],
    },
    purple: {
        color: theme.palette.getContrastText(deepPurple[500]),
        backgroundColor: deepPurple[500],
    },
}))

const Dashboard = () => {

    const classes1 = useStyles()

    // states
    const [fullName, setFullName] = useState('')
    const [mail, setMail] = useState('')
    const [ID, setID] = useState('')
    const [hasApplied, setHasApplied] = useState(false)
    const [isNavActive, setIsNavAcvtive] = useState(false)

    // run on every render
    useEffect(() => {
        const act = Cookie.get('accessToken')
        const applied = Cookie.get('secure_asp')
        const decodedData = jwtDecode(act)
        const { id, fullname, email } = decodedData
        applied && setHasApplied(true)
        setFullName(fullname)
        setMail(email)
        setID(id)
        (function () {
            var executed = false
            return function () {
                if (!executed) {
                    executed = true;
                    window.location = '/'
                }
            }
        })()
    }, [])

    const Auth = useContext(AuthApi)

    const handleLogout = async () => {
        const refreshToken = Cookie.get('refreshToken')
        const accessToken = Cookie.get('accessToken')

        try {
            await axios.post('/auth/logout', { token: refreshToken }, {
                headers: {
                    "token": `Bearer ${accessToken}`
                }
            })

            Auth.setAuth(false)
            Cookie.remove('accessToken')
            Cookie.remove('refreshToken')

        } catch (err) {
            console.log('error logging out')
        }
    }

    return (
        <section className="dashboard">
            <div className={`profile ${isNavActive ? 'active' : ''}`}>
                <div className="image">
                    <Avatar className={classes1.purple} style={{ justifySelf: 'end' }}>{fullName.charAt(0)}</Avatar>
                </div>

                <div className="data">
                    <small>Fullname</small>
                    <p
                        style={{
                            textOverflow: 'ellipsis', overflow: 'hidden', width: '160px', height: '1.2em', whiteSpace: 'nowrap'
                        }}>{fullName}</p>
                </div>


                <div className="data">
                    <small>Email</small>
                    <p style={{
                        textOverflow: 'ellipsis', overflow: 'hidden', width: '160px', height: '1.2em', whiteSpace: 'nowrap'
                    }}>{mail}</p>
                </div>


                <div className="data">
                    <small>Unique ID</small>
                    <Link to=""
                        style={{
                            textOverflow: 'ellipsis', overflow: 'hidden', width: '160px', height: '1.2em', whiteSpace: 'nowrap'
                        }}>{ID}</Link>
                </div>

                <div className="data">
                    <button onClick={handleLogout}>
                        Logout
                    </button>
                </div>
            </div>
            <div className="boards">
                <div className="top">
                    <button className="verified">
                        {hasApplied ?
                            <p>
                                <i className="fa fa-check" style={{ color: '#1dbf73', padding: '0 .3em' }}></i>
                                Data Supplied
                            </p>
                            :
                            <p>
                                <i className="fa fa-times" style={{ color: 'red', padding: '0 .5em' }}></i>
                                <Link to="/application">Not supplied</Link>
                            </p>
                        }
                    </button>

                    <i className="fa fa-bars menu" style={{ color: '#282F85', cursor: 'pointer' }} onClick={() => setIsNavAcvtive(!isNavActive)}></i>
                </div>

                <div className="dash">

                    <div className="project">
                        <small>Project:</small>
                        <Link to=""><h3>Victoria Trade Center</h3></Link>
                    </div>
                    <br />

                    <React.Fragment>
                        <Grid container spacing={3} style={{ marginBottom: '2em' }}>
                            <Grid item xs={12} lg={4} xl={4} md={4}>
                                <Paper style={{ padding: '1em', display: 'flex', alignItems: 'flex-start', height: '100px' }}>
                                    <small>Amount</small>
                                </Paper>
                            </Grid>
                            <Grid item xs={12} lg={4} xl={4} md={4}>
                                <Paper style={{ padding: '1em', display: 'flex', alignItems: 'flex-start', height: '100px' }}>
                                    <small>Paid</small>
                                </Paper>
                            </Grid>
                            <Grid item xs={12} lg={4} xl={4} md={4}>
                                <Paper style={{ padding: '1em', display: 'flex', alignItems: 'flex-start', height: '100px' }}>
                                    <small>Balance</small>
                                </Paper>
                            </Grid>
                        </Grid >
                    </React.Fragment >

                </div>
            </div>
        </section>
    )
}

export default Dashboard
