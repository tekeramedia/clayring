import React, { useState } from 'react'
import Header from "../components/Header"
import "../App.scss"
import Button from '../components/Button'
import axios from 'axios'
import { useHistory } from 'react-router-dom'
import Alert from '@material-ui/lab/Alert'

const Register = () => {

    const [fullname, setFullname] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState(false)
    const [buttonDisabled, setButtonDisabled] = useState(false)

    const history = useHistory()

    const handleRegister = async (e) => {
        e.preventDefault()
        setError(false)
        try {
            setButtonDisabled(true)
            const response = await axios.post('/auth/register', {
                fullname: fullname,
                email: email,
                password: password
            })
            console.log(response.data)
            history.push('/verify')
            setError(false)
        } catch (err) {
            setButtonDisabled(false)
            setError(true)
        }

    }

    return (
        <>
            <Header />

            <div class="form-modal">
                <h2>Register</h2>
                <form onSubmit={handleRegister} method="POST">
                    <div className="form-group">
                        <label htmlFor="">Fullname</label>
                        <input type="text" placeholder="Jane Doe" value={fullname} onChange={(e) => setFullname(e.target.value)} required />
                    </div>

                    <div className="form-group">
                        <label htmlFor="">Email</label>
                        <input type="email" placeholder="jane@gmail.com" value={email} onChange={(e) => setEmail(e.target.value)} required />
                    </div>

                    <div className="form-group">
                        <label htmlFor="">Password</label>
                        <input type="password" placeholder="********" value={password} onChange={(e) => setPassword(e.target.value)} required />
                    </div>

                    <div className="form-group">
                        <Button caption="Register" type="submit" width="inherit" disabled={buttonDisabled} />
                    </div>
                </form>
                <small style={{ textAlign: 'center' }}>Already have an account?<a href="/" style={{ paddingLeft: '10px' }}>Login</a></small>
                {error ? <div>
                    <Alert severity="error">This is an error alert — check it out!</Alert>
                </div>
                    : null}
            </div>

        </>
    )
}

export default Register
