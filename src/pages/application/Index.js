import React, { Component } from 'react'
import Personal from './components/Personal'
import Kin from './components/Kin'
import Employer from './components/Employer'
import Purchase from './components/Purchase'
import Payment from './components/Payment'
import Review from './components/Review'
import Success from './components/Success'


export class Application extends Component {

    state = {
        step: 1,
        fullname: "",
        address: "",
        cpaddress: "",
        gender: "",
        status: "",
        phone: "",
        mobile: "",
        email: "",
        kinFullname: "",
        kinPhone: "",
        kinEmail: "",
        kinAddress: "",
        empName: "",
        occupation: "",
        empTel: "",
        empAddress: "",
        shopUse: "",
        shopSelection: "First",
        agreedPrice: "",
        paymentMode: "",
        medium: "Friend",
        filled: false
    }

    next = () => {
        const { step } = this.state
        this.setState({
            step: step + 1
        })
    }

    previous = () => {
        const { step } = this.state
        this.setState({
            step: step - 1
        })
    }

    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    back = () => {
        const { step } = this.state
        this.setState({
            step: step - 5
        })
    }

    render() {
        const { step } = this.state
        
        const { fullname, address, cpaddress, gender, status, phone, mobile, email, kinFullname, kinPhone, kinAddress, kinEmail, empName, occupation, empTel, empAddress, shopUse, shopNumber, agreedPrice, paymentMode, medium } = this.state

        const values = { fullname, address, cpaddress, gender, status, phone, mobile, email, kinFullname, kinPhone, kinAddress, kinEmail, empName, occupation, empTel, empAddress, shopUse, shopNumber, agreedPrice, paymentMode, medium }

        switch (step) {
            case 1:
                return (
                    <Personal next={this.next} values={values} handleInputChange={this.handleInputChange} />
                )
            case 2:
                return (
                    <Kin next={this.next} previous={this.previous} values={values} handleInputChange={this.handleInputChange} />
                )
            case 3:
                return (
                    <Employer next={this.next} previous={this.previous} values={values} handleInputChange={this.handleInputChange} />
                )
            case 4:
                return <Purchase next={this.next} previous={this.previous} values={values} handleInputChange={this.handleInputChange} />
            case 5:
                return <Payment next={this.next} previous={this.previous} values={values} handleInputChange={this.handleInputChange} />
            case 6:
                return <Review back={this.back} next={this.next} previous={this.previous} values={values} handleInputChange={this.handleInputChange} />
            case 7:
                return <Success />
            default:
                return <h1>Application Form</h1>
        }
    }
}

export default Application
