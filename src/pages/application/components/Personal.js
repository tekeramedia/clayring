import React, { Component } from 'react'
import { Button, FormControl, FormControlLabel, FormLabel, MuiThemeProvider, Radio, RadioGroup, TextField, AppBar, Typography } from '@material-ui/core'

export class Personal extends Component {

    continue = (e) => {
        e.preventDefault()
        this.props.next()
    }

    render() {
        const { values, handleInputChange } = this.props

        return (
            <MuiThemeProvider>
                <React.Fragment>
                    <div style={{
                        display: 'flex', justifyContent: 'center', padding: '1em 0'
                    }}>
                        <AppBar title="Personal Details" style={{ padding: '15px' }}>
                            <Typography variant="p" align='center'>Supply Personal Details</Typography>
                        </AppBar>
                        <div style={{ paddingTop: '70px' }}>
                            <TextField id='standard-basic' label='Fullname' onChange={handleInputChange} defaultValue={values.fullname} name='fullname' required /><br />
                            <TextField id='standard-basic' label='Address' onChange={handleInputChange} defaultValue={values.address} name='address' required /> <br />
                            <TextField id='standard-basic' label='Adress 2' onChange={handleInputChange} defaultValue={values.cpaddress} name='cpaddress' required /> <br /> <br />
                            <FormControl component="fieldset">
                                <FormLabel component="legend">Gender</FormLabel>
                                {/* <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange}> */}
                                <RadioGroup aria-label="gender" name="gender" value={values.gender} onChange={handleInputChange} defaultValue={values.gender} required>
                                    <FormControlLabel value="female" control={<Radio />} label="Female" />
                                    <FormControlLabel value="male" control={<Radio />} label="Male" />
                                    <FormControlLabel value="other" control={<Radio />} label="Rather not say" />
                                    {/* <FormControlLabel value="disabled" disabled control={<Radio />} label="(Disabled option)" /> */}
                                </RadioGroup>
                            </FormControl> <br />
                            <FormControl component="fieldset">
                                <FormLabel component="legend">Status</FormLabel>
                                {/* <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange}> */}
                                <RadioGroup aria-label="gender" name="status" value={values.status} onChange={handleInputChange} defaultValue={values.status} required>
                                    <FormControlLabel value="single" control={<Radio />} label="Single" />
                                    <FormControlLabel value="married" control={<Radio />} label="Married" />
                                    <FormControlLabel value="undisclosed" control={<Radio />} label="Rather not say" />
                                </RadioGroup>
                            </FormControl> <br />
                            <TextField id='standard-basic' label='Phone Number' onChange={handleInputChange} defaultValue={values.phone} name='phone' required /><br />
                            <TextField id='standard-basic' label='Mobile No' onChange={handleInputChange} defaultValue={values.mobile} name='mobile' required /> <br />
                            <TextField id='standard-basic' label='Email' onChange={handleInputChange} defaultValue={values.email} name='email' required /> <br /><br />
                            <Button variant='contained' type='submit' color='primary' onClick={this.continue}>
                                Continue
                            </Button>
                        </div>
                    </div>
                </React.Fragment >
            </MuiThemeProvider >
        )
    }
}

export default Personal
