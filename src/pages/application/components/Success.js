import React, { Component } from 'react'
import { Button, AppBar, Typography, MuiThemeProvider } from '@material-ui/core'

export class Success extends Component {

    backToDashboard = (e) => {
        e.preventDefault()
        window.location = "/"
    }
    render() {
        return (
            <MuiThemeProvider>
                <React.Fragment>
                    <div style={{
                        display: 'flex', justifyContent: 'center', padding: '1em 0'
                    }}>
                        <AppBar title="Personal Details" style={{ padding: '15px' }}>
                            <Typography variant="p" align='center'>Application Successful</Typography>
                        </AppBar>
                        <div style={{ paddingTop: '100px' }}>


                            <Button variant='contained' color='primary' onClick={this.backToDashboard} style={{ marginRight: '1em' }}>
                                Back to dashboard
                            </Button>
                        </div>

                    </div>
                </React.Fragment >
            </MuiThemeProvider >
        )
    }
}

export default Success
