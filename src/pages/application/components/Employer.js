import React, { Component } from 'react'
import { Button, MuiThemeProvider, TextField, AppBar, Typography } from '@material-ui/core'

export class Employer extends Component {

    continue = (e) => {
        e.preventDefault()
        this.props.next()
    }

    prev = (e) => {
        e.preventDefault()
        this.props.previous()
    }

    render() {
        const { values, handleInputChange } = this.props

        return (
            <MuiThemeProvider>
                <React.Fragment>
                    <div style={{
                        display: 'flex', justifyContent: 'center', padding: '1em 0'
                    }}>
                        <AppBar title="Personal Details" style={{ padding: '15px' }}>
                            <Typography variant="p" align='center'>Supply Employer Details</Typography>
                        </AppBar>
                        <div style={{ paddingTop: '100px' }}>
                            <TextField id='standard-basic' label='Occupation' onChange={handleInputChange} defaultValue={values.occupation} name='occupation' required /> <br />
                            <TextField id='standard-basic' label='Employer Name' onChange={handleInputChange} defaultValue={values.empName} name='empName' required /><br />
                            <TextField id='standard-basic' label='Employer Telephone' onChange={handleInputChange} defaultValue={values.empTel} name='empTel' required /> <br />
                            <TextField id='standard-basic' label='Employer Address' onChange={handleInputChange} defaultValue={values.empAddress} name='empAddress' required /> <br /> <br />

                            <Button variant='contained' color='primary' onClick={this.continue} style={{ marginRight: '1em' }}>
                                Continue
                            </Button>
                            <Button variant='contained' onClick={this.prev}>
                                previous
                            </Button>
                        </div>
                    </div>
                </React.Fragment >
            </MuiThemeProvider >
        )
    }
}

export default Employer
