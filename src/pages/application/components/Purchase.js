import React, { Component } from 'react'
import { Button, FormControl, FormControlLabel, FormLabel, MuiThemeProvider, Radio, RadioGroup, AppBar, Typography } from '@material-ui/core'

export class Purchase extends Component {

    continue = (e) => {
        e.preventDefault()
        this.props.next()
    }

    prev = (e) => {
        e.preventDefault()
        this.props.previous()
    }

    render() {
        const { values, handleInputChange } = this.props

        return (
            <MuiThemeProvider>
                <React.Fragment>
                    <div style={{
                        display: 'flex', justifyContent: 'center', padding: '1em 0'
                    }}>
                        <AppBar title="Personal Details" style={{ padding: '15px' }}>
                            <Typography variant="p" align='center'>Supply Purchase Details</Typography>
                        </AppBar>
                        <div style={{ paddingTop: '100px' }}>
                            <FormControl component="fieldset">
                                <FormLabel component="legend">Shop Use</FormLabel>
                                <RadioGroup aria-label="shop use" name="shopUse" value={values.shopUse} onChange={handleInputChange} defaultValue={values.shopUse} required>
                                    <FormControlLabel value="Investment" control={<Radio />} label="Investment" />
                                    <FormControlLabel value="Business" control={<Radio />} label="Business" />
                                </RadioGroup>
                            </FormControl> <br />
                            <FormControl component="fieldset">
                                <FormLabel component="legend">Status</FormLabel>
                                <RadioGroup aria-label="shop selection" name="shopSelection" value={values.shopSelection} onChange={handleInputChange} defaultValue="First" required>
                                    <FormControlLabel value="First" control={<Radio />} label="First Choice" />
                                    <FormControlLabel value="Second Choice" control={<Radio />} label="Second Choice" />
                                </RadioGroup>
                            </FormControl> <br />


                            <Button variant='contained' color='primary' onClick={this.continue} style={{ marginRight: '1em' }}>
                                Continue
                            </Button>
                            <Button variant='contained' onClick={this.prev} >
                                previous
                            </Button>
                        </div>



                    </div>
                </React.Fragment >
            </MuiThemeProvider >
        )
    }
}

export default Purchase
