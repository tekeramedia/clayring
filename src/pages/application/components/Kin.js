import React, { Component } from 'react'
import { Button, MuiThemeProvider, TextField, AppBar, Typography } from '@material-ui/core'

export class Kin extends Component {

    continue = (e) => {
        e.preventDefault()
        this.props.next()
    }

    prev = (e) => {
        e.preventDefault()
        this.props.previous()
    }

    render() {
        const { values, handleInputChange } = this.props

        return (
            <MuiThemeProvider>
                <React.Fragment>
                    <div style={{
                        display: 'flex', justifyContent: 'center', padding: '1em 0'
                    }}>
                        <AppBar title="Personal Details" style={{ padding: '15px' }}>
                            <Typography variant="p" align='center'>Supply Next of Kin Details</Typography>
                        </AppBar>
                        <div style={{ paddingTop: '100px' }}>
                            <TextField id='standard-basic' label='Fullname' onChange={handleInputChange} defaultValue={values.kinFullname} name='kinFullname' required /><br />
                            <TextField id='standard-basic' label='Phone' onChange={handleInputChange} defaultValue={values.kinPhone} name='kinPhone' required /> <br />
                            <TextField id='standard-basic' label='Email' onChange={handleInputChange} defaultValue={values.kinEmail} name='kinEmail' required /> <br /> <br />
                            <TextField id='standard-basic' label='Address' onChange={handleInputChange} defaultValue={values.kinAddress} name='kinAddress' required /> <br /> <br />

                            <Button variant='contained' color='primary' onClick={this.continue} style={{ marginRight: '1em' }}>                                Continue
                            </Button>
                            <Button variant='contained' onClick={this.prev}>
                                previous
                            </Button>
                        </div>
                    </div>
                </React.Fragment >
            </MuiThemeProvider >
        )
    }
}

export default Kin
