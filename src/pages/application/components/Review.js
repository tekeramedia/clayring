import React, { Component } from 'react'
import { Button, MuiThemeProvider, AppBar, Typography, TextField } from
    '@material-ui/core'
import axios from 'axios'
import jwtDecode from 'jwt-decode'
import Cookie from 'js-cookie'
import Cookies from 'js-cookie'

const token = Cookie.get('accessToken')


export class Review extends Component {
    continue = (e) => {
        e.preventDefault()
        this.props.next()
    }

    prev = (e) => {
        e.preventDefault()
        this.props.previous()
    }

    render() {

        const { values: { fullname, address, cpaddress, gender, status, phone, mobile, email, kinFullname, kinPhone, kinEmail, kinAddress, empName, occupation, empTel, empAddress, shopUse, shopSelection, agreedPrice, paymentMode, medium }, back } = this.props

        // check for empty fields
        for (const [key, value] of Object.entries(this.props.values)) {
            if (value === "") {
                back()
            }
            else {
                console.log(key)
            }
        }

        // handle data submission

        const handleSubmit = async () => {
            const { id } = jwtDecode(token)
            try {
                await axios.post('/api/v1/info', {
                    fullname: fullname,
                    customerId: id,
                    address: address,
                    gender: gender,
                    cpaddress: cpaddress,
                    status: status,
                    phone: phone,
                    mobile: mobile,
                    email: email,
                    kinFullname: kinFullname,
                    kinPhone: kinPhone,
                    kinEmail: kinEmail,
                    kinAddress: kinAddress,
                    empName: empName,
                    occupation: occupation,
                    empTel: empTel,
                    empAddress: empAddress,
                    shopUse: shopUse,
                    shopSelect: shopSelection,
                    agreedPrice: agreedPrice,
                    paymentMode: paymentMode,
                    medium: medium
                }, {
                    headers: {
                        token: `Bearer ${token}`
                    }
                })

                Cookies.set('secure_asp', process.env.REACT_APP_APPLIED_TOKEN)
                this.props.next()

            }
            catch (err) {
            }

        }


        return (
            <MuiThemeProvider>
                <React.Fragment>
                    <div style={{
                        display: 'flex', justifyContent: 'center', padding: '1em 0'
                    }}>
                        <AppBar title="Personal Details" style={{ padding: '15px' }}>
                            <Typography variant="p" align='center'>Confirm Supplied Details</Typography>
                        </AppBar>

                        <div style={{ paddingTop: '70px' }}>
                            <h1>Personal Details</h1> <br />

                            <TextField id='standard-basic' label='Full Name' value={fullname} /> <br />

                            <TextField id='standard-basic' label='Gender' value={gender} /> <br />

                            <TextField id='standard-basic' label='Address' value={address} /> <br />

                            <TextField id='standard-basic' label='Address 2' value={cpaddress} /> <br />

                            <TextField id='standard-basic' label='Status' value={status} /> <br />

                            <TextField id='standard-basic' label='Phone' value={phone} /> <br />

                            <TextField id='standard-basic' label='Mobile' value={mobile} /> <br />

                            <TextField id='standard-basic' label='Email' value={email} /> <br /> <br />

                            <Button variant='contained' color='primary' onClick={handleSubmit} style={{ marginRight: '1em' }}>
                                Submit
                            </Button>
                            <Button variant='contained' onClick={this.prev} >
                                previous
                            </Button>
                        </div>
                    </div>
                </React.Fragment >
            </MuiThemeProvider >
        )
    }
}

export default Review
