import React, { Component } from 'react'
import { Button, FormControl, FormControlLabel, FormLabel, MuiThemeProvider, Radio, RadioGroup, AppBar, Typography, TextField } from '@material-ui/core'

export class Payment extends Component {

    continue = (e) => {
        e.preventDefault()
        this.props.next()
    }

    prev = (e) => {
        e.preventDefault()
        this.props.previous()
    }


    render() {

        const { values, handleInputChange } = this.props
        return (
            <MuiThemeProvider>
                <React.Fragment>
                    <div style={{
                        display: 'flex', justifyContent: 'center', padding: '1em 0'
                    }}>
                        <AppBar title="Personal Details" style={{ padding: '15px' }}>
                            <Typography variant="p" align='center'>Supply Payment Details</Typography>
                        </AppBar>
                        <FormControl required={true} style={{ paddingTop: '100px' }}>
                            <TextField id='standard-basic' label='Agreed Price' onChange={handleInputChange} defaultValue={values.agreedPrice} name='agreedPrice' required /> <br /> <br />
                            <FormControl component="fieldset">
                                <FormLabel component="legend">Payment Mode</FormLabel>
                                <RadioGroup aria-label="paymentMode" name="paymentMode" required value={values.paymentMode} onChange={handleInputChange} defaultValue={values.paymentMode}>
                                    <FormControlLabel value="installment" control={<Radio />} label="Installment" />
                                    <FormControlLabel value="Once" control={<Radio />} label="One time" />
                                </RadioGroup>
                            </FormControl> <br />
                            <Button variant='contained' color='primary' onClick={this.continue} style={{ marginRight: '1em' }}>
                                Confirm & Submit
                            </Button> <br />
                            <Button variant='contained' onClick={this.prev} >
                                previous
                            </Button>
                        </FormControl>



                    </div>
                </React.Fragment >
            </MuiThemeProvider >
        )
    }
}

export default Payment
