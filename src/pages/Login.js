import React, { useState, useContext } from 'react'
import axios from "axios"
import Cookie from "js-cookie"
import AuthApi from "../AuthContext"
import Header from "../components/Header"
import "../App.scss"
import Button from '../components/Button'
import { useHistory, Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import Alert from '@material-ui/lab/Alert'

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}))


const Login = () => {

    const classes = useStyles()

    const Auth = useContext(AuthApi)
    const history = useHistory()


    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState(false)
    const [success, setSuccess] = useState(false)
    const [buttonDisabled, setButtonDisabled] = useState(false)


    const handleLogin = async (e) => {
        e.preventDefault()
        setError(false)
        try {
            setButtonDisabled(true)
            const response = await axios.post('/auth/login', { email, password })
            const { accessToken, refreshToken } = response.data
            Auth.setAuth(true)
            Cookie.set('accessToken', accessToken)
            Cookie.set('refreshToken', refreshToken)
            setSuccess(true)
        } catch (err) {
            setError(true)
            setButtonDisabled(false)
            setTimeout(() => {
                return history.push('/')
            }, 2000)
        }
    }

    return (
        <>
            <Header />

            <div class="form-modal">
                {error ?
                    <div className={classes.root}>
                        <Alert severity="error">Error Logging in!</Alert>
                    </div>
                    : null
                }
                <br />
                {success ?
                    <div className={classes.root}>
                        <Alert severity="success">Login Successful</Alert>
                    </div>
                    : null}
                <h2>Login</h2>
                <form onSubmit={handleLogin}>
                    <div className="form-group">
                        <label htmlFor="">Email</label>
                        <input type="email" placeholder="username@example.com" value={email} onChange={(e) => setEmail(e.target.value)} required />
                    </div>
                    <div className="form-group">
                        <label htmlFor="">Password</label>
                        <input type="password" placeholder="*******" value={password} onChange={(e) => setPassword(e.target.value)} required />

                        <small><Link to="/forgot">Forgot password?</Link></small>
                    </div>
                    <div className="form-group">
                        <Button caption="Login" type="submit" width="inherit" disabled={buttonDisabled} />
                    </div>
                </form>
                <small style={{ textAlign: 'center' }}>Don't have an account?<a href="/register" style={{ paddingLeft: '10px' }}>Register</a></small>
            </div>


        </>
    )
}

export default Login
