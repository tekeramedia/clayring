import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import InputCode from './InputCode'
import './styles.scss'

const Verify = () => {

    const CODE_LENGTH = 4
    const [loading, setLoading] = useState(false)
    const [isCodeInputFilled, setIsCodeInputFilled] = useState(false)

    const isButtonDisable = loading || !isCodeInputFilled;

    const onCodeComplete = code => {
        // make inputs readonly for 5 seconds
        setLoading(true);
        setTimeout(() => setLoading(false), 5000)
    }

    const onChangeCodeInput = code => {
        const isFilled = code.length === CODE_LENGTH
        setIsCodeInputFilled(isFilled)
    }

    const proceed = (e) => {
        e.preventDefault()
        alert('verified')
        return
    }

    return (
        <div className='verify'>
            <div className="main">
                <div className="modal">
                    <div className="text">
                        <p>Verify Your Email</p>
                        <small>A four digit verification has been sent to your email</small>
                    </div>

                    <form className="form" onSubmit={proceed}>
                        <InputCode
                            length={CODE_LENGTH}
                            loading={loading}
                            onChange={onChangeCodeInput}
                            onComplete={onCodeComplete}
                        />
                        <button
                            className={isButtonDisable ? 'disabled' : 'enabled'}
                            disabled={isButtonDisable}>
                            PROCEED
                        </button>
                    </form>

                    <Link to='resend' style={{ padding: '1em' }}>Resend Code</Link>
                </div>
            </div>
        </div>
    )
}

export default Verify
