import React, { useEffect, useState, useRef } from 'react'

const InputCode = ({ length = 0, loading, onChange, onComplete }) => {
    const inputs = useRef({})
    const [code, setCode] = useState([...Array(length)].map(() => ''))
    const [currentIndex, setCurrentIndex] = useState(0)

    const processInput = (value, index) => {
        if (/[^0-9]/.test(value)) return

        let newCode = [...code]
        if (value.length > 1) {
            for (let i = 0; i < value.length; i++) {
                if (index + i < length) {
                    newCode[index + i] = value[i]
                }
            }
            setCode(newCode)
        } else {
            newCode[index] = value
            setCode(newCode)
            if (index !== length - 1 && !!value) {
                setCurrentIndex(currentIndex + 1)
            }
        }

        onChange(newCode.join(''))
        if (newCode.every(num => num !== '')) {
            onComplete(newCode.join(''))
        }
    }

    const focusOnCurrentInput = () => {
        inputs.current[currentIndex].focus()
        inputs.current[currentIndex].select()
    };

    useEffect(() => {
        focusOnCurrentInput()
    })

    const move = direction => {
        const newIndex = direction === 'left' ? currentIndex - 1 : currentIndex + 1
        if (newIndex > -1 && newIndex < length) {
            setCurrentIndex(newIndex)
        }
    }

    const onPaste = (e, index) => {
        e.preventDefault()
        const text = e.clipboardData.getData('Text')
        processInput(text, index)
    }

    const onKeyUp = (e, index) => {
        switch (e.code) {
            case 'ArrowLeft':
                e.preventDefault()
                move('left')
                break

            case 'ArrowRight':
                e.preventDefault()
                move('right')
                break

            case 'Backspace':
                e.preventDefault();
                if (!code[index]) {
                    move('left');
                    processInput('', index - 1);
                } else {
                    processInput('', index)
                }
                break

            default:
                break
        }
    }

    const onFocus = index => {
        setCurrentIndex(index)
    }

    return (
        <div className="input-boxes">
            {code.map((value, index) => {
                return (
                    <input
                        key={index}
                        type="text"
                        inputMode="numeric"
                        autoComplete="off"
                        maxLength={1}
                        value={value}
                        autoFocus={!code[0].length && index === 0}
                        onChange={e => processInput(e.target.value, index)}
                        onKeyDown={e => onKeyUp(e, index)}
                        ref={ref => (inputs.current[index] = ref)}
                        onFocus={() => onFocus(index)}
                        onPaste={e => onPaste(e, index)}
                        disabled={loading}
                    />
                )
            })}
        </div>
    )
}

export default InputCode
