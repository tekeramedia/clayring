import React, { useState } from "react"
import axios from "axios"
import Header from "../components/Header"
import "../App.scss"
import Button from '../components/Button'
import { useHistory } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import Alert from '@material-ui/lab/Alert'

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}))


const Forgot = () => {

    const classes = useStyles()

    const history = useHistory()

    const [email, setEmail] = useState('')
    const [error, setError] = useState(false)
    const [success, setSuccess] = useState(false)
    const [buttonDisabled, setButtonDisabled] = useState(false)


    const handleRequest = async (e) => {
        e.preventDefault()
        setError(false)
        try {
            setButtonDisabled(true)
            await axios.post('/auth/forgot', { email })
            setSuccess(true)
        } catch (err) {
            setError(true)
            setButtonDisabled(false)
            setTimeout(() => {
                return history.push('/')
            }, 2000)
        }
    }

    return (
        <>
            <Header />
            <div class="form-modal">
                {error ?
                    <div className={classes.root}>
                        <Alert severity="error">Error Resseting Password</Alert>
                    </div>
                    : null
                }
                <br />
                {success ?
                    <div className={classes.root}>
                        <Alert severity="success">Passwprd Reset Successful</Alert>
                    </div>
                    : null}
                <h2>Reset Password</h2>
                <form onSubmit={handleRequest}>
                    <div className="form-group">
                        <label htmlFor="">Email</label>
                        <input type="email" placeholder="username@example.com" value={email} onChange={(e) => setEmail(e.target.value)} required />
                    </div>
                    <div className="form-group">
                        <Button caption="Reset" type="submit" width="inherit" disabled={buttonDisabled} />
                    </div>
                </form>
                <small style={{ textAlign: 'center' }}>Don't have an account?<a href="/register" style={{ paddingLeft: '10px' }}>Register</a></small>
            </div>
        </>
    )
}

export default Forgot
