import { Route, Redirect } from "react-router-dom"

const ProtectedForgot = ({ auth, component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={
                () => !auth ? (
                    < Component />
                ) : (
                    <Redirect to="/dashboard" />
                )
            }
        />
    )
}

export default ProtectedForgot
