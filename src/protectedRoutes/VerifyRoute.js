import { Route, Redirect } from "react-router-dom"

const ProtectedVerify = ({ auth, component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={
                () => !auth ? (
                    < Component />
                ) : (
                    <Redirect to="/" />
                )
            }
        />
    )
}

export default ProtectedVerify
