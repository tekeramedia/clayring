import { Route, Redirect } from "react-router-dom"
import Cookie from 'js-cookie'

const token = Cookie.get('accessToken')
const ProtectedApplication = ({ auth, component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={
                () => token ? (
                    <Component />
                ) :
                    (
                        <Redirect to='/' />
                    )
            }
        />
    )
}

export default ProtectedApplication
