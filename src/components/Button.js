import React from 'react'
import propTypes from 'prop-types'

const Button = ({ background, caption, color, height, width, type, disabled }) => {
    return (
        <>
            <button type={type} disabled={disabled}>{caption}</button>
            <style jsx>
                {`
                    button {
                        background: ${background};
                        border-radius: 5px;
                        cursor: pointer;
                        border: none;
                        font-family: 'Poppins';
                        font-size: 16px;
                        color: ${color};
                        height: ${height}px;
                        width: ${width}px;
                    }

                    button:hover {
                        background: #252957;
                        transition: .5s;
                    }
                `}
            </style>
        </>

    )
}

Button.defaultProps = {
    background: '#282f85',
    caption: 'Get Started',
    color: '#fff',
    height: '50',
    width: '250',
}

Button.propTypes = {
    background: propTypes.string,
    caption: propTypes.string,
    color: propTypes.string,
    height: propTypes.string,
    width: propTypes.string,
}

export default Button
