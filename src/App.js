import Cookie from "js-cookie"
import React, { useContext, useEffect, useState } from "react"
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import "./App.scss"
import AuthApi from "./AuthContext"
import Application from "./pages/Application"
import Dashboard from "./pages/Dashboard"
import Login from "./pages/Login"
import Register from "./pages/Register"
import Verify from "./pages/Verify"
import Forgot from "./pages/Forgot"
import ProtectedApplication from "./protectedRoutes/ApplicationRoute"
import ProtectedDashboard from "./protectedRoutes/DashboardRoute"
import ProtectedForgot from "./protectedRoutes/ForgotRoute"
import ProtectedLogin from "./protectedRoutes/LoginRoute"
import ProtectedRegister from "./protectedRoutes/RegisterRoute"
import ProtectedVerify from "./protectedRoutes/VerifyRoute"

function App() {

  const [auth, setAuth] = useState(false)

  const authorizeUser = () => {
    const user = Cookie.get('accessToken')
    user && setAuth(true)
  }

  useEffect(() => {
    authorizeUser()
  }, [])

  return (
    <div className="App">
      <AuthApi.Provider value={{ auth, setAuth }}>
        <Router>
          <Routes />
        </Router>
      </AuthApi.Provider>
    </div>
  )
}

const Routes = () => {
  const AuthContext = useContext(AuthApi)

  return (
    <Switch>
      <ProtectedLogin exact path="/" component={Login} auth={AuthContext.auth} />
      <ProtectedVerify path="/verify" auth={AuthContext.auth} component={Verify} />
      <ProtectedForgot path="/forgot" auth={AuthContext.auth} component={Forgot} />
      <ProtectedDashboard path="/dashboard" auth={AuthContext.auth} component={Dashboard} />
      <ProtectedRegister path="/register" auth={AuthContext.auth} component={Register} />
      <ProtectedApplication path="/application" component={Application} />
    </Switch>
  )
}

export default App
