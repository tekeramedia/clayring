import React, { createContext, useEffect, useState } from 'react'
import axios from "axios"
import { useState } from "react"
import jwtDecode from "jwt-decode"

const [email, setEmail] = useState('')
const [password, setPassword] = useState('')
const [user, setUser] = useState(null)


const refreshToken = async () => {
    try {
        const res = await axios.post("/auth/refresh", { token: user.refreshToken })
        setUser({
            ...user,
            accessToken: res.data.accessToken,
            refreshToken: res.data.refreshToken
        })
        return res.data
    } catch (err) {
        console.log(err)
    }
}

const handleSubmit = async (e) => {
    e.preventDefault()

    try {
        const res = await axios.post("/auth/login", { email, password })
        setUser(res.data)
    } catch (err) {
        console.log('something is wrong', err)
    }
}

// axios instance
const axiosAuth = axios.create()

axiosAuth.interceptors.request.use(
    async (config) => {
        let currentDate = new Date()
        const decodedToken = jwtDecode(user.accessToken)
        if (decodedToken.exp * 1000 < currentDate.getTime()) {
            const data = await refreshToken()
            config.headers['token'] = 'Bearer ' + data.accessToken
        }
        return config
    }, (err) => {
        return Promise.reject(err)
    })

const handleDelete = async (id) => {
    try {
        await axiosAuth.delete("/auth/delete/" + id, {
            headers: { token: "Bearer " + user.accessToken }
        })
    } catch (err) {
        console.log(err)
    }
}



{
    !user &&
        <form onSubmit={handleSubmit}>
            <input type="email" name="" placeholder="email" value={email} onChange={(e) => setEmail(e.target.value)} />
            <input type="password" name="" placeholder="password" value={password} onChange={(e) => setPassword(e.target.value)} />
            <button type="submit">Login</button>
        </form>
}

{
    user ? <div>
        <h1>Welcome {user.email} {user.accessToken}</h1>
        <button onClick={() => handleDelete(user.id)}>Delete</button>
    </div> : ''
}
// const ProtectedDashboard = ({ auth, component: Component,...rest }) => {
//   return (
//     <Route
//       {...rest}
//       render = {
//         () => auth ? (
//           < Component />
//         ) : (
//             <Redirect to="/"/>
//          )
//     }
//     />
//   )
// }

// const ProtectedLogin = ({ auth, component: Component,...rest }) => {
//   return (
//     <Route
//       {...rest}
//       render = {
//         () => !auth ? (
//           < Component />
//         ) : (
//             <Redirect to="/dashboard"/>
//          )
//     }
//     />
//   )
// }
